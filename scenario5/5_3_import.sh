#!/bin/bash
sqoop-export --connect jdbc:mysql://localhost/retail_db --username retail_dba --password cloudera \
	--table products_external --export-dir /user/hive/warehouse/problem5.db/products_hive \
	--update-mode allowinsert \
	--update-key product_id \
 	--fields-terminated-by  \
	--input-null-string "null" \
	--input-null-non-string "null" 

hive --database problem5 \
	 -e "insert into products_hive (product_id) select max(product_id) + 1 from products_hive; insert into products_hive (product_id) select max(product_id) + 1 from products_hive;"
sqoop-export --connect jdbc:mysql://localhost/retail_db --username retail_dba --password cloudera \
	--table products_external --export-dir /user/hive/warehouse/problem5.db/products_hive \
	--fields-terminated-by  \ 
	--update-mode allowinsert \
	--update-key product_id \
	--input-null-string "null" \
	--input-null-non-string "null" 

