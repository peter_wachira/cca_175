import com.databricks.spark.avro._

val o = sqlContext.read.avro("/user/cloudera/problem1/orders").
		withColumn("order_date_dt",from_unixtime($"order_date"/1000,"yyyy-MM-dd")).
		drop("order_date").
		withColumnRenamed("order_date_dt","order_date")

val oi = sqlContext.read.avro("/user/cloudera/problem1/order_items")
import org.apache.spark.sql.Row

val aggRdd = o.join(oi,$"order_id" === $"order_item_order_id").
		select($"order_date",$"order_status",$"order_item_subtotal".as("amount"),$"order_id").
		map{case Row (order_date: String, order_status: String, amount: Float, order_id: Int) =>
			 ((order_date, order_status),(amount,order_id))}.
		combineByKey((v: (Float, Int)) =>(v._1, Set(v._2)),
			(acc1:(Float,Set[Int]),v:(Float, Int)) => (acc1._1 + v._1,(acc1._2 + v._2)),
			(acc1: (Float,Set[Int]), acc2:( Float, Set[Int])) => (acc1._1+acc2._1,acc1._2 ++ acc2._2 ))


case class aggClass (order_date: String, order_status: String, total_amount: Float, count_orders: Int)

val aggDF = aggRdd.map{v =>aggClass( v._1._1, v._1._2,v._2._1,v._2._2.size)}.toDF()
aggDF.write.parquet("/user/cloudera/problem1/result4c-gzip")
